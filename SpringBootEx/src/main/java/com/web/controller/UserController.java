package com.web.controller;

import com.core.model.entities.User;
import com.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/demo")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/index")
    public String index(){
        System.out.print("requested index page");
        return "index";
    }

    @GetMapping(path="/add")
    public @ResponseBody String addNewUser (@RequestParam String name
            , @RequestParam String email) {

        User n = new User();
        n.setName(name);
        n.setEmail(email);
        userRepository.save(n);

        return "\n Saved --> " + n.toString();
    }

    @GetMapping(path="/all")
    public @ResponseBody String getAllUsers() {

        System.out.print("\nGetting user list...\n");

        String returnString = userRepository.findAll().stream()
                .map(user -> user.toString())
                .collect(Collectors.joining("\n"));

        System.out.print(returnString + "\n");

        return returnString;
    }

    @GetMapping(path="/getbyname")
    public @ResponseBody String getByName (@RequestParam String name) {

        System.out.print(String.format("\nSearching user by name %s...\n", name));

        String returnString = userRepository.findByName(name).stream()
                .map(user -> user.toString())
                .collect(Collectors.joining("\n"));

        System.out.print(returnString + "\n");

        return returnString;
    }
}
