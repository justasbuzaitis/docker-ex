package com.core.model;

public abstract class AbstractEntity<T> {

    private static final int ODD_PRIME = 31;

    public abstract T getId();

    @Override
    public int hashCode() {
        return getId() == null ? 0 : ODD_PRIME * getId().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (getId() == null || obj == null || !(getClass().equals(obj.getClass()))) {
            return false;
        }

        return getId().equals(((AbstractEntity) obj).getId());
    }
}

