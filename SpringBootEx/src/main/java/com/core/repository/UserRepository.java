package com.core.repository;

import com.core.model.entities.User;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface UserRepository extends Repository<User, Integer> {

    User save(User user);

    List<User> findAll();

    List<User> findByName(String name);
}